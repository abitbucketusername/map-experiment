#include <iostream>
#include <conio.h>
#define UP_ARROW 72
#define DOWN_ARROW 80
#define LEFT_ARROW 75
#define RIGHT_ARROW 77
#define ONE_KEY 49
#define TWO_KEY 50

using namespace std;

void displayMap();
void move();

int xPos = 0, yPos = 0;
char playerInput;
string message = "You are in a room.", key = "0";

char map[9][9] = {{' ',' ',' ',' ',' ',' ',' ',' ',' '},
                    {' ',' ',' ',' ',' ',' ',' ',' ',' '},
                    {' ',' ',' ',' ',' ',' ',' ',' ',' '},
                    {' ',' ',' ','I',' ',' ',' ',' ',' '},
                    {' ',' ',' ',' ',' ',' ',' ',' ',' '},
                    {' ',' ',' ',' ',' ',' ',' ',' ',' '},
                    {' ',' ',' ',' ',' ',' ',' ',' ',' '},
                    {' ',' ',' ',' ',' ',' ',' ',' ',' '},
                    {' ',' ',' ',' ',' ',' ',' ',' ',' '}};
                    
char display[9][9] = {{' ',' ',' ',' ',' ',' ',' ',' ',' '},
                    {' ',' ',' ',' ',' ',' ',' ',' ',' '},
                    {' ',' ',' ',' ',' ',' ',' ',' ',' '},
                    {' ',' ',' ',' ',' ',' ',' ',' ',' '},
                    {' ',' ',' ',' ',' ',' ',' ',' ',' '},
                    {' ',' ',' ',' ',' ',' ',' ',' ',' '},
                    {' ',' ',' ',' ',' ',' ',' ',' ',' '},
                    {' ',' ',' ',' ',' ',' ',' ',' ',' '},
                    {' ',' ',' ',' ',' ',' ',' ',' ',' '}};

int main() {
    while(playerInput!=99){
         displayMap();
         move();
    }
}

void displayMap(){
     system("CLS");
     
     for(int i = 0; i <= 8; i++){
        for(int n = 0; n <= 8; n++){
            if(i == yPos && n == xPos){
                display[i][n] = 'O';
            } else {
                display[i][n] = map[i][n];
            }
        }
     }
     
     for(int x = 0; x <= 8; x++){
         for(int z = 0; z <= 8; z++){
             cout << display[x][z];
         }
         cout << endl;
     }
     
     cout << message << endl;
}

void inventory(){
    message = "Inventory: Keys " + key;     
}

void interact(){
     if(map[yPos][xPos] == 'I'){
       message = "You found a key.";
       key = "1";
       map[yPos][xPos] = ' ';
     } else {
       message = "You are in a room.";       
     }
}

void move(){
     playerInput = getch();
     switch(playerInput){
          case LEFT_ARROW:  
               if(xPos != 0){
                    xPos--;
                    message = "You Move left.";
               }else{ message = "You hit a wall.";} 
               break;
          case RIGHT_ARROW: 
               if(xPos != 8){
                    xPos++;
                    message = "You Move right."; 
               }else{ message = "You hit a wall.";} 
               break;
          case UP_ARROW: 
               if(yPos != 0){
                    yPos--;
                    message = "You Move up.";
               }else{ message = "You hit a wall.";} 
               break;
          case DOWN_ARROW:
               if(yPos != 8){
                    yPos++;
                    message = "You Move down."; 
               }else{ message = "You hit a wall.";} 
               break;
          case ONE_KEY:
               interact();
               break;
          case TWO_KEY:
               inventory();
               break;
    default: break;              
}
}
